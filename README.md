# Study Graphana
This repo is for studing grafana for made plots.

## How to run

```
docker-compose up
# localhost:3000
# admin, admin
```

## References
* docker, docker compose
* graphana
  * [Graphana docker example](https://github.com/curityio/grafana/tree/master)
* [CSV Dataset from kaggle](https://grafana.com/grafana/plugins/cloudspout-button-panel/)